﻿// <auto-generated />
using System;
using AthletesAPI.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace AthletesAPI.Migrations
{
    [DbContext(typeof(AthletesDbContext))]
    [Migration("20230616095006_seeding_athletes")]
    partial class seeding_athletes
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.7")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("AthletesAPI.Models.Athlete", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int?>("CoachId")
                        .HasColumnType("int");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.HasIndex("CoachId");

                    b.ToTable("Athletes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            FirstName = "Keenan",
                            LastName = "Cornelius"
                        },
                        new
                        {
                            Id = 2,
                            FirstName = "Gordan",
                            LastName = "Ryan"
                        },
                        new
                        {
                            Id = 3,
                            FirstName = "Tommy",
                            LastName = "Langaker"
                        },
                        new
                        {
                            Id = 4,
                            FirstName = "Lucas",
                            LastName = "Barbosa"
                        });
                });

            modelBuilder.Entity("AthletesAPI.Models.Coach", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Coaches");
                });

            modelBuilder.Entity("AthletesAPI.Models.Athlete", b =>
                {
                    b.HasOne("AthletesAPI.Models.Coach", "Coach")
                        .WithMany()
                        .HasForeignKey("CoachId");

                    b.Navigation("Coach");
                });
#pragma warning restore 612, 618
        }
    }
}
