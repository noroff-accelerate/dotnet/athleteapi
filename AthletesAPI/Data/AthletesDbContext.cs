﻿using AthletesAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace AthletesAPI.Data
{
    public class AthletesDbContext : DbContext
    {
        public AthletesDbContext(DbContextOptions options): base(options)
        {
        }

        public DbSet<Athlete> Athletes { get; set; }
        public DbSet<Coach> Coaches { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Athlete>().HasData(
                new Athlete() { Id = 1, FirstName = "Keenan", LastName = "Cornelius" },
                new Athlete() { Id = 2, FirstName = "Gordan", LastName = "Ryan" },
                new Athlete() { Id = 3, FirstName = "Tommy", LastName = "Langaker" },
                new Athlete() { Id = 4, FirstName = "Lucas", LastName = "Barbosa" });

        }
    }
}
