﻿using AthletesAPI.Models;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace AthletesAPI.Data.DTOs.AthleteDTOs
{
    public class AthleteCreateDTO
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
       
       
    }
}
