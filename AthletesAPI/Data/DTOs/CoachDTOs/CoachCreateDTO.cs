﻿using System.ComponentModel.DataAnnotations;

namespace AthletesAPI.Data.DTOs.CoachDTOs
{
    public class CoachCreateDTO
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
    }
}
