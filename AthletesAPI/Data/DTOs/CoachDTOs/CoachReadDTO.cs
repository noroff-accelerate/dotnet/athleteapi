﻿using AthletesAPI.Models;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace AthletesAPI.Data.DTOs.CoachDTOs
{
    public class CoachReadDTO
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [AllowNull]
        public int[] Athletes { get; set; }
    }
}
