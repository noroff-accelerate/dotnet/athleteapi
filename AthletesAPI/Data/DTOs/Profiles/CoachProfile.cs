﻿using AthletesAPI.Data.DTOs.CoachDTOs;
using AthletesAPI.Models;
using AutoMapper;

namespace AthletesAPI.Data.DTOs.Profiles
{
    public class CoachProfile : Profile
    {
        public CoachProfile()
        {
            CreateMap<CoachCreateDTO, Coach>();
            CreateMap<Coach, CoachReadDTO>()
                .ForMember(
                readDto => readDto.Athletes, 
                opt => opt.MapFrom(
                    domain => domain.Athletes.Select(a=> a.Id).ToArray()));
            CreateMap<CoachUpdateDTO,Coach>();

        }
    }
}
