﻿using AthletesAPI.Data.DTOs.AthleteDTOs;
using AthletesAPI.Models;
using AutoMapper;

namespace AthletesAPI.Data.DTOs.Profiles
{
    public class AthleteProfile : Profile
    {

        public AthleteProfile() {
            CreateMap<Athlete, AthleteReadDTO>();
            CreateMap<AthleteCreateDTO, Athlete >();
            CreateMap<AthleteUpdateDTO, Athlete>();
        }
    }
}
