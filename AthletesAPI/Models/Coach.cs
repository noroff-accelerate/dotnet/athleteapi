﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace AthletesAPI.Models
{
    public class Coach
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [AllowNull]
        public ICollection<Athlete>? Athletes { get; set;}
    }
}
