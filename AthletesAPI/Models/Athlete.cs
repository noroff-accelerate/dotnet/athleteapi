﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace AthletesAPI.Models
{
    public class Athlete
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string  LastName { get; set; }
        [AllowNull]
        public int? CoachId { get; set; }
        [AllowNull]
        public Coach? Coach { get; set; }
    }
}
