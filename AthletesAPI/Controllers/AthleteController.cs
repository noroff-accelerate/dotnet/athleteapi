﻿using AthletesAPI.Data;
using AthletesAPI.Data.DTOs.AthleteDTOs;
using AthletesAPI.Models;
using AutoMapper;
using MessagePack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Validations;

namespace AthletesAPI.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AthleteController : ControllerBase
    {
        private readonly AthletesDbContext _context;
        private readonly IMapper _mapper;

        public AthleteController(AthletesDbContext context, IMapper mapper){
            _context = context;
            _mapper = mapper;

            }
       
        // READ
        /// <summary>
        /// Gets all athletes
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]/all")]
        public async Task<ActionResult<List<AthleteReadDTO>>> GetAll()
        {
            // Query db
            var athleteList = await _context.Athletes.ToListAsync<Athlete>();

            // convert into athlete read dtos

            var atheleteDtoList = _mapper.Map<List<AthleteReadDTO>>(athleteList);

            // return list of athletes

            return Ok(atheleteDtoList);
        }

        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<AthleteReadDTO>> GetById(int id)
        {
            // Query db
            var domainAthlete = await _context.Athletes.FindAsync(id);

            if(domainAthlete == null)
            {
                return NotFound();
            }

            // convert to DTO
            var athleteDTO = _mapper.Map<AthleteReadDTO>(domainAthlete);

            // return list of athletes

            return Ok(athleteDTO);
        }

        // CREATE
        [HttpPost]
        public async Task<ActionResult<AthleteReadDTO>> CreateAthlete([FromBody] AthleteCreateDTO newAthleteDTO)
        {

            if (ModelState.IsValid == true)
            {
                var newDomainAthelete = _mapper.Map<Athlete>(newAthleteDTO);

                await _context.Athletes.AddAsync(newDomainAthelete);
                await _context.SaveChangesAsync();

                var readDTO = _mapper.Map <AthleteReadDTO>(newDomainAthelete);
                return CreatedAtAction(nameof(GetById), new { id = readDTO.Id }, readDTO);
            }
            else
            {
                return BadRequest();
            }

            

            // created 
            
        }

        // UPDATE
        [HttpPut]
        public async Task<ActionResult> UpdateAthlete(int id, [FromBody] AthleteUpdateDTO updatedAthlete)
        {
            if(id != updatedAthlete.Id)
            {
                return BadRequest();
            }

            if (ModelState.IsValid == true)
            {
                var domainAthlete = _mapper.Map<Athlete>(updatedAthlete);

            _context.Entry(domainAthlete).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
            }
            else
            {
                return BadRequest("Object is invalid");
            }

        }

        // DELETE
        [HttpDelete("[action]/{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            // Query db
            var athlete = await _context.Athletes.FindAsync(id);

            if(athlete == null)
            {
                return NotFound();
            }

            _context.Athletes.Remove(athlete);

            await _context.SaveChangesAsync();

            // return list of athletes

            return NoContent();
        }

        // CUSTOM USE CASE ENDPOINTS


        /// <summary>
        /// Assigns a coach to an athlete
        /// </summary>
        /// <param name="id">Athletes Id</param>
        /// <param name="coachId">Id of the coach being assigned</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("assignCoach/{id}")]
        public async Task<ActionResult> AssignCoachToAthlete(int id, [FromBody]int coachId)
        {
            var domainAthlete = await _context.Athletes.FindAsync(id);

            var domainCoach = await _context.Coaches.FindAsync(coachId);

            if(domainCoach == null)
            {
                return BadRequest("No coach to assign in DB with that ID");
            }

            domainAthlete.CoachId = coachId;

            await _context.SaveChangesAsync();

            return NoContent();
        }


       
    }
}
