﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AthletesAPI.Data;
using AthletesAPI.Models;
using AthletesAPI.Data.DTOs.AthleteDTOs;
using AutoMapper;
using AthletesAPI.Data.DTOs.CoachDTOs;

namespace AthletesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoachesController : ControllerBase
    {
        private readonly AthletesDbContext _context;
        private readonly IMapper _mapper;

        public CoachesController(AthletesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Coaches
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoachReadDTO>>> GetCoaches()
        {
          if (_context.Coaches == null)
          {
              return NotFound();
          }

            var coaches = await _context.Coaches.Include(c=> c.Athletes).ToListAsync();

            var readCoaches = _mapper.Map<List<CoachReadDTO>>(coaches);

            return readCoaches;
        }

        // GET: api/Coaches/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CoachReadDTO>> GetCoach(int id)
        {
          if (_context.Coaches == null)
          {
              return NotFound();
          }
            var coach = await _context.Coaches.
                            Where(c => c.Id == id)
                           .Include(c => c.Athletes)
                           .FirstOrDefaultAsync();

            var coachReadDto = _mapper.Map<CoachReadDTO>(coach);

            if (coach == null)
            {
                return NotFound();
            }

            return coachReadDto;
        }

        // PUT: api/Coaches/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoach(int id, CoachUpdateDTO coachDto)
        {
            if (id != coachDto.Id)
            {
                return BadRequest();
            }

            var domainCoach = _mapper.Map<Coach>(coachDto);

            _context.Entry(domainCoach).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoachExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Coaches
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Coach>> PostCoach(CoachCreateDTO newCoachDto)
        {

            var domainCoach = _mapper.Map<Coach>(newCoachDto);

            _context.Coaches.Add(domainCoach);
            await _context.SaveChangesAsync();

            var coachReadtDTO = _mapper.Map<CoachReadDTO>(domainCoach);

            return CreatedAtAction("GetCoach", new { id = coachReadtDTO.Id }, coachReadtDTO);
        }

        // DELETE: api/Coaches/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCoach(int id)
        {
            if (_context.Coaches == null)
            {
                return NotFound();
            }
            var coach = await _context.Coaches.FindAsync(id);
            if (coach == null)
            {
                return NotFound();
            }

            _context.Coaches.Remove(coach);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CoachExists(int id)
        {
            return (_context.Coaches?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        // Get all athletes for a coach

        [HttpGet("coach/athletes/{id}")]
        public async Task<ActionResult<IEnumerable<AthleteReadDTO>>> GetAthletesOfCoach(int id)
        {
            var domainCoach = _context.Coaches.
                            Where(c => c.Id == id)
                           .Include(c => c.Athletes)
                           .FirstOrDefault();

            if (domainCoach == null)
            {
                return NotFound();
            }

            var coachesAthletes = _mapper.Map<List<AthleteReadDTO>>(domainCoach.Athletes);


            return Ok(coachesAthletes);
        }



    }
}
